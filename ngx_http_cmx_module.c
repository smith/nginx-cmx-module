/**
 * NGINX module that publishes CMX metrics in the Prometheus format.
 *
 * Directives:
 *  -cmx: Returns the CMX metrics in the Prometheus format. This directive supports an optional
 *        HTTP argument 'component' that can be used to filter out metrics by a CMX component name.
 *
 * This module is HEAVILY inspired by the NGINX hello world example:
 * https://github.com/perusio/nginx-hello-world-module
 *
 * (c) 2019 CERN
 */

#include <inttypes.h> // PRId64
#include <math.h> // isnan()
#include <stdio.h>
#include <stdlib.h>

#include <cmw-cmx/cmx.h>
#define CMW_CMX_USE_PRIVATE
#include <cmw-cmx/shm-private.h>

#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>

#define BUFFER_SIZE 1048576 // Maximum response size: 1Mb
#define METRIC_BUFFER_SIZE 512 // Maximum size for a single metric
#define CMX_SHM_STRING_BUFFER_SIZE 256 // Buffer size to get data from 'cmx_shm_get_value_string'

static const char COMPONENT_ARGUMENT[] = "component=";
static const size_t COMPONENT_ARGUMENT_LENGTH = sizeof(COMPONENT_ARGUMENT) - 1;
static const char PROCESS_NAME_ARGUMENT[] = "process_name=";
static const size_t PROCESS_NAME_ARGUMENT_LENGTH = sizeof(PROCESS_NAME_ARGUMENT) - 1;

// The buffer for storing a response
static u_char response_buffer[BUFFER_SIZE];

// Buffer to get data from 'cmx_shm_get_value_string'
static char cmx_shm_string_buffer[CMX_SHM_STRING_BUFFER_SIZE];
static size_t bufpos = 0;
static void cmx_str_callback(const char * d, size_t s, void* v)
{
    memcpy(cmx_shm_string_buffer, d, s);
    bufpos = s;
}

static char *ngx_http_cmx(ngx_conf_t *cf, ngx_command_t *cmd, void *conf);
static ngx_int_t ngx_http_cmx_handler(ngx_http_request_t *r);

/**
 * This module provided directive: cmx.
 */
static ngx_command_t ngx_http_cmx_commands[] =
{
    {
        ngx_string("cmx"), // directive
        NGX_HTTP_LOC_CONF | NGX_CONF_NOARGS, // location context and takes no arguments
        ngx_http_cmx, // configuration setup function
        0, // no offset, only one context is supported
        0, // no offset when storing the module configuration on struct
        NULL
    },

    ngx_null_command // command termination
};

/*
 * The module context.
 */
static ngx_http_module_t ngx_http_cmx_module_ctx =
{
    NULL, // preconfiguration
    NULL, // postconfiguration

    NULL, // create main configuration
    NULL, // init main configuration

    NULL, // create server configuration
    NULL, // merge server configuration

    NULL, // create location configuration
    NULL // merge location configuration
};

/*
 * Module definition.
 */
ngx_module_t ngx_http_cmx_module =
{
    NGX_MODULE_V1, &ngx_http_cmx_module_ctx, // module context
    ngx_http_cmx_commands, // module directives
    NGX_HTTP_MODULE, // module type
    NULL, // init master
    NULL, // init module
    NULL, // init process
    NULL, // init thread
    NULL, // exit thread
    NULL, // exit process
    NULL, // exit master
    NGX_MODULE_V1_PADDING
};

/*
 * Replace all '.' in src string by '_'. This is needed because Prometheus doesn't support '.' characters
 * in the metric names.
 */
static void sanify_cmx_name(const char* src, char* dest)
{
    size_t i = 0;
    size_t size_src = strlen(src);
    for (i = 0; i < size_src; i++)
    {
        if (src[i] == '.')
        {
            dest[i] = '_';
        }
        else
        {
            dest[i] = src[i];
        }
    }
    dest[size_src] = '\0';
}


static ngx_int_t init_cmx_discover(ngx_log_t* log, cmx_component_info** components, int* component_count)
{
    int result = cmx_registry_discover(components, component_count);

    if (result != E_CMX_SUCCESS)
    {
        ngx_log_error(NGX_LOG_ERR, log, 0, "Failed to call cmx_registry_discover.");
        return NGX_HTTP_INTERNAL_SERVER_ERROR;
    }

    return NGX_HTTP_OK;
}

/**
 * Gets process id by process name
 */
static int get_process_id(ngx_log_t* log, const cmx_component_info* components, const int component_count, ngx_str_t process_name)
{
    int i, j;
    for (i = 0; i < component_count; i++)
    {
        if (strcmp(components[i].name, "_") == 0)
        {
            cmx_shm* cmx_shm_ptr;
            if (cmx_shm_open_ro(components[i].process_id, components[i].name, &cmx_shm_ptr) != E_CMX_SUCCESS)
            {
                ngx_log_error(NGX_LOG_WARN, log, 0, "Skipped component '%s': cmx_shm_open_ro failed.", components[i].name);
                return NGX_HTTP_OK;
            }
            for (j = 0; j < CMX_NUMBER_OF_VALUES; j++)
            {
                if (cmx_shm_ptr->value_state[j] == CMX_SLOT_STATE_SET || cmx_shm_ptr->value_state[j] == CMX_SLOT_STATE_UPDATE)
                {
                    if (strcmp(cmx_shm_ptr->value[j].value.name, "process_name") == 0)
                    {
                        int value_handle = 0;
                        int value_type = 0;
                        cmx_shm_find_value(cmx_shm_ptr, j, &value_handle, &value_type);
                        cmx_shm_get_value_string(cmx_shm_ptr, value_handle, cmx_str_callback, NULL, NULL );
                        if (strlen(cmx_shm_string_buffer) == process_name.len &&
                                strncmp(cmx_shm_string_buffer, (char*) process_name.data, process_name.len) == 0)
                        {
                            int process_id = components[i].process_id;
                            cmx_shm_unmap(cmx_shm_ptr);
                            return process_id;
                        }
                    }
                }
            }
            cmx_shm_unmap(cmx_shm_ptr);
        }
    }

    return -1;
}

/**
 * Writes a single CMX entry in the provided buffer. Returns a size of 0 in case of failure.
 */
static size_t write_cmx_entry(ngx_log_t* log, char* buffer, const cmx_shm_slot_value* value, const char* component_name)
{
    ngx_log_error(NGX_LOG_DEBUG, log, 0, "Adding value '%s'.", value->name);

    char metric_name[strlen(value->name) + 1];
    sanify_cmx_name(value->name, metric_name);

    size_t size = snprintf(buffer, METRIC_BUFFER_SIZE, "%s{component=\"%s\"} ", metric_name, component_name);

    switch (value->type)
    {
    case CMX_TYPE_INT64:
        size += snprintf(buffer + size, METRIC_BUFFER_SIZE - size, "%" PRId64 "\n", value->value._int64);
        break;
    case CMX_TYPE_FLOAT64:
        if (isnan(value->value._float64))
        {
            size += snprintf(buffer + size, METRIC_BUFFER_SIZE - size, "NaN\n");
        }
        else
        {
            size += snprintf(buffer + size, METRIC_BUFFER_SIZE - size, "%lf\n", value->value._float64);
        }
        break;
    case CMX_TYPE_BOOL:
        size += snprintf(buffer + size, METRIC_BUFFER_SIZE - size, value->value._bool ? "1\n" : "0\n");
        break;
    default:
        ngx_log_error(NGX_LOG_WARN, log, 0, "Skipped metric '%s': Unsupported type %d.", value->name, value->type);
        return 0;
    }

    if (size > METRIC_BUFFER_SIZE)
    {
        ngx_log_error(NGX_LOG_WARN, log, 0, "Skipped metric '%s': Metric buffer overflow.", value->name);
        return 0;
    }

    return size;
}

static ngx_int_t write_component(ngx_log_t* log, char* response_buffer, size_t* response_size, const cmx_component_info component)
{
    cmx_shm* cmx_shm_ptr;
    if (cmx_shm_open_ro(component.process_id, component.name, &cmx_shm_ptr) != E_CMX_SUCCESS)
    {
        ngx_log_error(NGX_LOG_WARN, log, 0, "Skipped component '%s': cmx_shm_open_ro failed.", component.name);
        return NGX_HTTP_OK;
    }

    ngx_log_error(NGX_LOG_DEBUG, log, 0, "Handling component '%s'.", component.name);
    char component_name[strlen(component.name) + 1];
    sanify_cmx_name(component.name, component_name);

    int j;
    char metric_buffer[METRIC_BUFFER_SIZE];
    for (j = 0; j < CMX_NUMBER_OF_VALUES; j++)
    {
        if (cmx_shm_ptr->value_state[j] == CMX_SLOT_STATE_SET || cmx_shm_ptr->value_state[j] == CMX_SLOT_STATE_UPDATE)
        {
            size_t metric_size = write_cmx_entry(log, metric_buffer, &cmx_shm_ptr->value[j].value, component_name);

            if ((BUFFER_SIZE - *response_size) <= metric_size)
            {
                cmx_shm_unmap(cmx_shm_ptr);
                ngx_log_error(NGX_LOG_ERR, log, 0, "Output buffer overflow.");
                return NGX_HTTP_INTERNAL_SERVER_ERROR;
            }

            memcpy(response_buffer + *response_size, metric_buffer, metric_size);
            *response_size += metric_size;
        }
    }

    cmx_shm_unmap(cmx_shm_ptr);

    return NGX_HTTP_OK;
}

/**
 * Reads all CMX metrics and write them in response_buffer.
 */
static ngx_int_t read_all_cmx_metrics(ngx_log_t* log, char* response_buffer, size_t* response_size, const cmx_component_info* components,
        const int component_count)
{
    int i;
    for (i = 0; i < component_count; i++)
    {
        ngx_int_t status = write_component(log, response_buffer, response_size, components[i]);
        if (status != NGX_HTTP_OK)
        {
            return status;
        }
    }

    return NGX_HTTP_OK;
}

/**
 * Reads all CMX metrics that match with a specific component name and write them in response_buffer.
 */
static ngx_int_t read_cmx_metrics_by_component(ngx_log_t* log, char* response_buffer, size_t* response_size,
        const cmx_component_info* components, const int component_count, const ngx_str_t component_argument)
{
    int i;
    for (i = 0; i < component_count; i++)
    {
        size_t component_size = strlen(components[i].name);
        if (component_argument.len != component_size || strncmp((char*) component_argument.data,  components[i].name, component_size) != 0)
        {
            continue;
        }

        ngx_int_t status = write_component(log, response_buffer, response_size, components[i]);

        if (status != NGX_HTTP_OK)
        {
            return status;
        }
    }

    return NGX_HTTP_OK;
}

/**
 * Reads all CMX metrics that match with a specific component name and write them in response_buffer.
 */
static ngx_int_t read_cmx_metrics_by_process(ngx_log_t* log, char* response_buffer, size_t* response_size, const cmx_component_info* components,
        const int component_count, const ngx_str_t process_argument)
{
    int process_id = get_process_id(log, components, component_count, process_argument);

    if (process_id == -1)
    {
        ngx_log_error(NGX_LOG_ERR, log, 0, "Failed finding the process '%s' in CMX.", (char*) process_argument.data);
        return NGX_HTTP_INTERNAL_SERVER_ERROR;
    }

    int i;
    for (i = 0; i < component_count; i++)
    {
        if (process_id != components[i].process_id)
        {
            continue;
        }
        ngx_int_t status = write_component(log, response_buffer, response_size, components[i]);
        if (status != NGX_HTTP_OK)
        {
            return status;
        }
    }

    return NGX_HTTP_OK;
}

/**
 * Get and parse nginx http argument to a 'ngx_str_t' structure
 */
static void get_nginx_argument(ngx_str_t* nginx_argument, char* data_http_argument, char* whitespace_position, int argument_length)
{
    nginx_argument->len = whitespace_position - data_http_argument - argument_length;
    nginx_argument->data = (unsigned char*) data_http_argument + argument_length;
}

/**
 * Content handler.
 *
 * @param r
 *   Pointer to the request structure. See http_request.h.
 * @return
 *   The status of the response generation.
 */
static ngx_int_t ngx_http_cmx_handler(ngx_http_request_t *r)
{
    ngx_buf_t *b;
    ngx_chain_t out;

    // Set the Content-Type header.
    r->headers_out.content_type.len = sizeof("text/plain") - 1;
    r->headers_out.content_type.data = (u_char *) "text/plain";

    // Allocate a new buffer for sending out the reply.
    b = ngx_pcalloc(r->pool, sizeof(ngx_buf_t));
    if (b == NULL)
    {
        ngx_log_error(NGX_LOG_ERR, r->connection->log, 0, "Failed to allocate response buffer.");
        return NGX_HTTP_INTERNAL_SERVER_ERROR;
    }

    // Insertion in the buffer chain.
    out.buf = b;
    out.next = NULL; // just one buffer

    ngx_log_error(NGX_LOG_INFO, r->connection->log, 0, "Reading CMX registry...");

    size_t response_size = 0;
    ngx_int_t result = NGX_HTTP_OK;

    cmx_component_info* components = NULL;
    int component_count;
    result = init_cmx_discover(r->connection->log, &components, &component_count);

    if (result != NGX_HTTP_OK)
    {
        return result;
    }

    ngx_str_t nginx_argument;
    nginx_argument.data = NULL;
    if (r->args.data != NULL)
    {
        char* whitespace_position = strchr((char*)r->args.data, ' ');
        if (whitespace_position == NULL)
        {
            ngx_log_error(NGX_LOG_ERR, r->connection->log, 0, "Failed to get http arguments, wrong arguments.");
            return NGX_HTTP_INTERNAL_SERVER_ERROR;
        }

        if (strncmp((char*)r->args.data, COMPONENT_ARGUMENT, COMPONENT_ARGUMENT_LENGTH) == 0)
        {
            get_nginx_argument(&nginx_argument, (char*)r->args.data, whitespace_position, COMPONENT_ARGUMENT_LENGTH);
            result = read_cmx_metrics_by_component(r->connection->log, (char*) response_buffer, &response_size, components, component_count, nginx_argument);
        }
        else if (strncmp((char*)r->args.data, PROCESS_NAME_ARGUMENT, PROCESS_NAME_ARGUMENT_LENGTH) == 0)
        {
            get_nginx_argument(&nginx_argument, (char*)r->args.data, whitespace_position, PROCESS_NAME_ARGUMENT_LENGTH);
            result = read_cmx_metrics_by_process(r->connection->log, (char*) response_buffer, &response_size, components, component_count, nginx_argument);
        }
        else
        {
            ngx_log_error(NGX_LOG_ERR, r->connection->log, 0, "Failed to get http arguments, wrong arguments.");
            return NGX_HTTP_INTERNAL_SERVER_ERROR;
        }

    }
    else
    {
        result = read_all_cmx_metrics(r->connection->log, (char*) response_buffer, &response_size, components, component_count);
    }

    free(components);

    if (result != NGX_HTTP_OK)
    {
        return result;
    }

    ngx_log_error(NGX_LOG_INFO, r->connection->log, 0, "Finished to read CMX values, response_size=%d.", response_size);

    // nginx doesn't like empty responses (logs an error).
    // Set the buffer to a single byte to avoid this. The content length being 0 gives an empty body to the client anyway.
    if (response_size == 0)
    {
        // Handle empty response differently
        b->pos = (u_char*)"\n";
        b->last = b->pos + 1;
    }
    else
    {
        b->pos = response_buffer; // first position in memory of the data
        b->last = response_buffer + response_size; // last position in memory of the data
    }
    b->memory = 1; // content is in read-only memory
    b->last_buf = 1; // there will be no more buffers in the request

    // Sending the headers for the reply.
    r->headers_out.status = NGX_HTTP_OK; // 200 status code
    // Get the content length of the body.
    r->headers_out.content_length_n = response_size;
    ngx_http_send_header(r); /* Send the headers */

    // Send the body, and return the status code of the output filter chain.
    return ngx_http_output_filter(r, &out);
}

/**
 * Configuration setup function that installs the content handler.
 *
 * @param cf
 *   Module configuration structure pointer.
 * @param cmd
 *   Module directives structure pointer.
 * @param conf
 *   Module configuration structure pointer.
 * @return string
 *   Status of the configuration setup.
 */
static char *ngx_http_cmx(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
    ngx_http_core_loc_conf_t *clcf; // pointer to core location configuration

    // Install the handler.
    clcf = ngx_http_conf_get_module_loc_conf(cf, ngx_http_core_module);
    clcf->handler = ngx_http_cmx_handler;

    return NGX_CONF_OK;
}
