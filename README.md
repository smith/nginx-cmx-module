# nginx-cmx-module

NGINX module for exposing CMX metrics through HTTP using Prometheus format

## Building the module

In order to build the dynamic module, NGINX must be properly configured. This is how to configure the CMX module for the NGINX 1.12.2 as compiled in the [Fedora Epel repo](https://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/n/).

1) Download the NGINX 1.12 sources: http://nginx.org/download/nginx-1.12.2.tar.gz

2) If needed, install the required dependencies with this command (or disable the building of the modules which require them with `--without-module_name`; `configure` will tell you the module name): `yum install gperftools-devel gd-devel libxslt-devel libxml2-devel`

2) Configure NGINX

It is recommended to get the exact configure command line from the default nginx installation, to avoid incompatibilities:

* Connect to any FEC
* Run `nginx -V` and copy the command line
* Add `-I/acc/local/L867/cmw/cmw-cmx/2.1.7/include` to the `--with-cc-opt` flag. Example: `--with-cc-opt='-I/acc/local/L867/cmw/cmw-cmx/2.1.7/include -O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -m64 -mtune=generic'`
* Add `-L/acc/local/L867/cmw/cmw-cmx/2.1.7/lib -lcmw-cmx -lrt` at the end of the `--with-ld-opt` flag. Example: `--with-ld-opt='-Wl,-z,relro -specs=/usr/lib/rpm/redhat/redhat-hardened-ld -Wl,-E -L/acc/local/L867/cmw/cmw-cmx/2.1.7/lib -lcmw-cmx -lrt'`
* Add `--add-dynamic-module=/path/to/nginx-cmx-module` at the end of the command (replace the path with the correct one).

An example of the full command line is below. This makes the assumption the CMX module is located in parent folder of NGINX.

Reminder: you should not use this command line as is, as newer version of nginx may be configured differently, and it could cause wrong runtime behaviour in the worst case.

```
./configure --prefix=/usr/share/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib64/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --http-client-body-temp-path=/var/lib/nginx/tmp/client_body --http-proxy-temp-path=/var/lib/nginx/tmp/proxy --http-fastcgi-temp-path=/var/lib/nginx/tmp/fastcgi --http-uwsgi-temp-path=/var/lib/nginx/tmp/uwsgi --http-scgi-temp-path=/var/lib/nginx/tmp/scgi --pid-path=/run/nginx.pid --lock-path=/run/lock/subsys/nginx --user=nginx --group=nginx --with-file-aio --with-ipv6 --with-http_auth_request_module --with-http_ssl_module --with-http_v2_module --with-http_realip_module --with-http_addition_module --with-http_sub_module --with-http_dav_module --with-http_flv_module --with-http_mp4_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_random_index_module --with-http_secure_link_module --with-http_degradation_module --with-http_slice_module --with-http_stub_status_module --with-mail_ssl_module --with-pcre --with-pcre-jit --with-stream=dynamic --with-stream_ssl_module --with-debug --with-cc-opt='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -m64 -mtune=generic -I/acc/local/L867/cmw/cmw-cmx/2.1.7/include' --with-ld-opt='-Wl,-z,relro -specs=/usr/lib/rpm/redhat/redhat-hardened-ld -Wl,-E -L/acc/local/L867/cmw/cmw-cmx/2.1.7/lib -lcmw-cmx -lrt' --add-dynamic-module=../nginx-cmx-module
```

3) Build the modules
```
make modules
```

This should create the *objs/ngx_http_cmx_module.so* dynamic module.
